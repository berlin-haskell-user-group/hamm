# Version 3
This is the final version of `hamm`. It basically is just a the same as
version 2 using the [singletons](https://hackage.haskell.org/package/singletons)
package to avoid boilerplate code. Which means we have enable lots of extensions

```haskell
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE EmptyCase #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoCUSKs #-}
{-# LANGUAGE NoStarIsType #-}
```

and add two import

```haskell
import Data.Singletons
import Data.Singletons.TH
import Data.Kind (Type)
import Text.Read (readMaybe)
import Data.Maybe (fromMaybe, mapMaybe)
import Data.Char (toLower)
import Data.List.Extra
import Data.Foldable
```

Now we can let `singletons` generate the singletons data type.
```haskell
$(singletons [d|
  data Category =
     Food
   | Rent
   | Fun
   | Misc
   deriving (Eq, Enum, Bounded, Show)
  |])
```

We can even generate the `SDecide` instance.
```haskell
-- $(singDecideInstance ''Category)
```

```haskell
parseCategory :: String -> Category
parseCategory str = fromMaybe Misc $ find ((== str) . fmap toLower . show) (enumerate @Category)
```

```haskell
data Expense :: Category -> Type where
  Expense :: Float -> Expense c
```

```haskell
instance Semigroup (Expense c) where
  Expense x <> Expense y = Expense (x + y)

instance Monoid (Expense c) where
  mempty = Expense 0
```

```haskell
filterExpense :: SCategory c -> [SomeExpense] -> [Expense c]
filterExpense _ [] = []
filterExpense c ((SomeExpense xCat xExpense) : xs) = case c %~ xCat of
  (Proved Refl) -> xExpense : filterExpense c xs
  (Disproved _) -> filterExpense c xs
```

```haskell
parseExpense :: String -> Maybe (Expense c)
parseExpense str = Expense <$> readMaybe str
```

```haskell
data SomeExpense = forall c.
  SomeExpense
  { someExpenseCurrency :: SCategory c,
    someExpense :: Expense c
  }
```

```haskell
parseSomeExpense :: String -> Maybe SomeExpense
parseSomeExpense str = case words str of
  expenseStr:categoryStr:_ -> do
    let category = parseCategory categoryStr
    withSomeSing category $ \sCategory -> do
      expense <- parseExpense expenseStr
      Just $ SomeExpense sCategory expense
  _ -> Nothing

```

```haskell
showExpense :: SCategory a -> Expense a -> String
showExpense c (Expense x) = show x ++ " " ++ show (fromSing c)
```

```haskell
main :: IO ()
main = do
  expenses <- mapMaybe parseSomeExpense . lines <$> getContents
  forM_ (enumerate @Category) $ \category ->
    withSomeSing category (\cat -> putStrLn . showExpense cat . fold . filterExpense cat $ expenses)
```
