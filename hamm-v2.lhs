# Version 2
Reimplemetation of `hamm` using `DataKinds` to promote `Category` to a kind, so
we can use categories as a type parameter.

For the following code to work we have to enable some language extensions.
```haskell
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ExplicitNamespaces #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RankNTypes #-}
```

```haskell
import Text.Read (readMaybe)
import Data.Char (toLower)
import Data.List.Extra ( enumerate )
import Data.Foldable ( Foldable(fold), forM_, find )
import Data.Maybe (mapMaybe, fromMaybe)
```

Also some additional imports are required.
```haskell
import Data.Kind (Type)
import Data.Type.Equality (type (:~:)(Refl), testEquality, TestEquality)
```

We start with the same `Category`
```haskell
data Category =
     Food
   | Rent
   | Fun
   | Misc
   deriving (Eq, Enum, Bounded, Show)

parseCategory :: String -> Category
parseCategory str = fromMaybe Misc $ find ((== str) . fmap toLower . show) enumerate
```

Using `GADTs` we can rewrite `Expense` to add a `Category` as type parameter.
```haskell
data Expense :: Category -> Type where
  Expense :: Float -> Expense c
```

Unfortunately we using `GADTs` we cannot derive anymore and have write a custom `show` function.
See below.

Now we can rewrite `(.+.)` without error handling.
```haskell
(.+.) :: Expense c -> Expense c -> Expense c
Expense x .+. Expense y = Expense (x + y)
```
The signature of `(.+.)` means that both expense have to have the
category `c` as parameter **and** the resulting expense keeps the
category.

For every `Category` `c` we can actually make `Expense c` a `Monoid`.
```haskell
instance Semigroup (Expense c) where
  (<>) = (.+.)

instance Monoid (Expense c) where
  mempty = Expense 0
```

```haskell
parseExpense :: String -> Maybe (Expense c)
parseExpense str = Expense <$> readMaybe str
```

Having a closer look at `parseExpense` might make you wonder.
We now have to know the category `c` at compile time. But that's an
information we want to read from the input at runtime. So we need a
data type that gives us the information at runtime.

```haskell
data SomeExpense' = forall c.
  SomeExpense'
  { someExpenseCategory' :: Category,
    someExpense' :: Expense c
  }
```
The `forall c` indicates an existential qualified type. Which basically means
when you use the type you know it exists a category `c` but you don't know which.

This looks quite the same as the Agda implementation. Unfortunately this won't help us.
```haskell
funExpense :: Expense 'Fun
funExpense = Expense 5

strangeExpense :: SomeExpense'
strangeExpense = SomeExpense' Rent funExpense
```
… oh oh. Haskell does not relate the category of `SomeExpense'` with its expense.
Where in Agda we could just use the value of the category, in Haskell we have to
define a new data type which is also parameterized over a category **and** given
it's constructor we can deduce the parameter. This can be done by defining a *single*
constructor for every category. This is called a singleton type.

```haskell
data SCategory :: Category -> Type where
  SFood :: SCategory 'Food
  SRent :: SCategory 'Rent
  SFun :: SCategory 'Fun
  SMisc :: SCategory 'Fun
```

```haskell
data SomeExpense = forall c.
  SomeExpense
  { someExpenseCategory :: SCategory c,
    someExpense :: Expense c
  }
```

Now this code fails
```
invalidExpense :: SomeExpense
invalidExpense = SomeExpense SRent funExpense
```

with
```
• Couldn't match type ‘'Fun’ with ‘'Rent’
  Expected type: Expense 'Rent
    Actual type: Expense 'Fun
```

Haskell is right of course. Rent is not fun.

Note that for every singleton we can obtain the actual value
```haskell
fromSing :: SCategory c -> Category
fromSing SFood = Food
fromSing SRent = Rent
fromSing SFun = Fun
fromSing SMisc = Misc
```

but the opposite is not possible
```
toSing :: Category -> SCategory c
toSing Food = SFood
...
```

since the caller would be able to chose the `c` which would give contradictions as above, e.g.
```
myFun :: SCategory Fun
myFun = SRent
```

To define `toSing` we would need to define an existential `SomeCategory`, which we won't here.

However if you cave a `Category` and are stuck with a function on `SCategory` you can use
```haskell
withSomeSing :: Category -> (forall c. SCategory c -> r) -> r
withSomeSing Food f = f SFood
withSomeSing Rent f = f SRent
withSomeSing Fun f = f SFun
withSomeSing Misc f = f SMisc
```

Note that the function `f` is existential on `c` which means `f` knows there exists a
`c` it has to handle but doesn't know which, i.e. it has to be prepared `forall`.


```haskell
parseSomeExpense :: String -> Maybe SomeExpense
parseSomeExpense str = case words str of
  expenseStr:categoryStr:_ -> do
    let category = parseCategory categoryStr
    withSomeSing category $ \sCategory -> do
      expense <- parseExpense expenseStr
      Just $ SomeExpense sCategory expense
  _ -> Nothing

```

Given all that `Some`thing we finally know how to use `SomeExpense` as *runtime witness*
for the parameter of `Expense`.

```haskell
isFunExpense :: SomeExpense -> Maybe (Expense 'Fun)
isFunExpense (SomeExpense SFun expense) = Just expense
isFunExpense _ = Nothing
```

Note that matching on e.g. `SRent` we again would get
```
• Couldn't match type ‘'Rent’ with ‘'Fun’
  Expected type: Maybe (Expense 'Fun)
    Actual type: Maybe (Expense c)
```

Now we can write a type level version of `filterExpense`, which does not only filter
a list of `SomeExpense` but also tell the compiles the parameter.
```haskell
filterExpense :: SCategory c -> [SomeExpense] -> [Expense c]
filterExpense _ [] = []
filterExpense c ((SomeExpense xCat xExpense) : xs) = case c `testEquality` xCat of
  Just Refl -> xExpense : filterExpense c xs
  Nothing -> filterExpense c xs
```

You can think of `testEquality` as a more generic way as the pattern match used in `isFunExpense`.
We could have written `filterExpense` using pattern matching on every combination of `xCat` and `c`
which would be quite tedious. This is what `testEquality` is for.

```haskell
instance TestEquality SCategory where
  testEquality SFood SFood = Just Refl
  testEquality SFood SRent = Nothing
  testEquality SFood SFun = Nothing
  testEquality SFood SMisc = Nothing
  testEquality SRent SFood = Nothing
  testEquality SRent SRent = Just Refl
  testEquality SRent SFun = Nothing
  testEquality SRent SMisc = Nothing
  testEquality SFun SFood = Nothing
  testEquality SFun SRent = Nothing
  testEquality SFun SFun = Just Refl
  testEquality SFun SMisc = Nothing
  testEquality SMisc SFood = Nothing
  testEquality SMisc SRent = Nothing
  testEquality SMisc SFun = Nothing
  testEquality SMisc SMisc = Just Refl
```

As promised before here is our custom show function for expenses.
```haskell
showExpense :: SCategory a -> Expense a -> String
showExpense c (Expense x) = show x ++ " " ++ show (fromSing c)
```

```haskell
main :: IO ()
main = do
  expenses <- mapMaybe parseSomeExpense . lines <$> getContents
  forM_ enumerate $ \category ->
    -- To access type level programming we have to use singletons and write an existential lambda.
    withSomeSing category (\cat -> putStrLn . showExpense cat . fold . filterExpense cat $ expenses)
```

Note that using Haskell type system not only make error handling explicit but also redundant.
