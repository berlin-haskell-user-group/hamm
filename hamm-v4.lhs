# Version 4
Samm as version 4 but using `Text`/`Symbol` as category.

```haskell
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE EmptyCase #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneKindSignatures #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoCUSKs #-}
{-# LANGUAGE NoStarIsType #-}
{-# LANGUAGE OverloadedStrings #-}
```

```haskell
import qualified Data.Text as T
import Data.Singletons ( withSomeSing, SingKind(fromSing) )
import Data.Kind (Type)
import Text.Read (readMaybe)
import Data.Maybe (mapMaybe)
import Data.Foldable ( Foldable(fold), forM_ )
import Data.Singletons.Prelude (Symbol)
import Data.Singletons.TypeLits (SSymbol)
import Data.Singletons.Decide (Decision(Proved, Disproved),type  (:~:) (Refl), (%~))
```

```haskell
data Expense :: Symbol -> Type where
  Expense :: Float -> Expense c
```

```haskell
instance Semigroup (Expense c) where
  Expense x <> Expense y = Expense (x + y)

instance Monoid (Expense c) where
  mempty = Expense 0
```

```haskell
filterExpense :: SSymbol c -> [SomeExpense] -> [Expense c]
filterExpense _ [] = []
filterExpense c ((SomeExpense xCat xExpense) : xs) = case c %~ xCat of
  (Proved Refl) -> xExpense : filterExpense c xs
  (Disproved _) -> filterExpense c xs
```

```haskell
parseExpense :: String -> Maybe (Expense c)
parseExpense str = Expense <$> readMaybe str
```

```haskell
data SomeExpense = forall c.
  SomeExpense
  { someExpenseCurrency :: SSymbol c,
    someExpense :: Expense c
  }
```

```haskell
parseSomeExpense :: String -> Maybe SomeExpense
parseSomeExpense str = case words str of
  expenseStr:categoryStr:_ -> do
    let category = T.pack categoryStr
    withSomeSing category $ \sCategory -> do
      expense <- parseExpense expenseStr
      Just $ SomeExpense sCategory expense
  _ -> Nothing

```

```haskell
showExpense :: SSymbol a -> Expense a -> String
showExpense c (Expense x) = show x ++ " " ++ T.unpack (fromSing c)
```

```haskell
main :: IO ()
main = do
  expenses <- mapMaybe parseSomeExpense . lines <$> getContents
  forM_ [T.pack "fun", "rent", "food", "misc", "clothes"] $ \category ->
    withSomeSing category (\cat -> putStrLn . showExpense cat . fold . filterExpense cat $ expenses)
```
