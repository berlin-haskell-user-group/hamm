# Version 1
The first "naive" implementation of `hamm` which add two expenses in `Maybe`.

```haskell
import Text.Read (readMaybe)
import Data.Char (toLower)
import Data.List.Extra ( find, enumerate )
import Data.Foldable ( forM_ )
import Data.Maybe (mapMaybe, fromMaybe)
import Control.Monad (foldM)
```

Define the categories to group expenses by.
```haskell
data Category =
     Food
   | Rent
   | Fun
   | Misc
   deriving (Eq, Enum, Bounded, Show)
```

An `Expense` it then a `category` together with an `amount`.
```haskell
data Expense = Expense { category :: Category, amount :: Float } deriving (Show)
```

We want to "sum up" expenses, which we will reduce to the basic case of adding
two expenses. Note that we want to make sure that we only add `Expense`s when
they have the same category. We could just let the application crash when they
don't.

```haskell ignore
(.+.) :: Expense -> Expense -> Expense
Expense xCategory xAmount .+. Expense yCategory yAmount =
  if xCategory == yCategory
    then Expense xCategory $ xAmount + yAmount
    else error "category missmatch"
```

But since we are all nice Haskell developers, reading `error` gives us a heart
attack. So we encode the error in a `Maybe` which btw. gives the caller the
opportunity to chose a proper error handling instead of just letting someone elses
code crash.
```haskell
(.+.) :: Expense -> Expense -> Maybe Expense
Expense xCategory xAmount .+. Expense yCategory yAmount =
  if xCategory == yCategory
    then Just . Expense xCategory $ xAmount + yAmount
    else Nothing
```

As final piece of logic we need a function to filter expenses by category.
```haskell
filterExpense :: Category -> [Expense] -> [Expense]
filterExpense _ [] = []
filterExpense c (x@(Expense xCat _) : xs) =
  if c == xCat
    then x : filterExpense c xs
    else filterExpense c xs
```

For IO we implement a very basic parser for `Category` which falls back
to `Misc` and a parser for `Money` on top.
```haskell
parseCategory :: String -> Category
parseCategory str = fromMaybe Misc $ find ((== str) . fmap toLower . show) enumerate

parseExpense :: String -> Maybe Expense
parseExpense str = case words str of
  expenseStr:categoryStr:_ -> Expense (parseCategory categoryStr) <$> readMaybe expenseStr
  _ -> Nothing
```

Glue it all together in `main`.
```haskell
main :: IO ()
main = do
  expenses <- mapMaybe parseExpense . lines <$> getContents
  forM_ enumerate $ \cat -> print . foldM (.+.) (Expense cat 0) . filterExpense cat $ expenses
```

We used `foldM` instead of `foldr` because `(.+.)` lives in the `Maybe` Monad.

A sample output might look like this
```
$ cabal run exe:hamm-v1 <<EOF
20      fun
12      food
17      clothes
4       food
10000   rent
EOF
Just (Expense {category = Food, amount = 16.0})
Just (Expense {category = Rent, amount = 10000.0})
Just (Expense {category = Fun, amount = 20.0})
Just (Expense {category = Misc, amount = 17.0})
```

Note that every output is wrapped in a `Just` all because `(.+.)` **might** fail.
This is is a bit cumbersome. Of course we could implement `(.+.)` using `error`
since we know the application is safe as a side effect of calling `filterExpense`
and risk a crash when the application might change.

With Haskell we can also have another option [hamm-v2.lhs](hamm-v2.lhs).
