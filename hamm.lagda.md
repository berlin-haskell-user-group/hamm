```
import Agda.Builtin.IO using (IO)
import Data.Unit.Polymorphic.Base using (⊤)
import Level
open import IO using (IO)
open import Data.Unit using (⊤)
import Data.List as List
open import Data.List as List using (mapMaybe; List; []; _∷_)
open import Data.String as String using (String; lines)
open import Data.Maybe using (Maybe; just; nothing)
open import Function using (_∘_ ; _$_)
open import Data.Nat using (ℕ)
open import Relation.Binary.PropositionalEquality
open import Relation.Binary.PropositionalEquality.Properties as ≡
open import Relation.Binary

data Category : Set where
  food : Category
  rent : Category
  fun : Category
  misc : Category

categoryIsPropositionalSetoid : Setoid Level.zero Level.zero
categoryIsPropositionalSetoid =
  record {
    Carrier = Category;
    _≈_ = _≡_;
    isEquivalence = ≡.isEquivalence
  }

open import Relation.Binary using (Decidable)
open import Relation.Nullary using (yes; no)
_≟_ : Decidable {A = Category} _≡_
food ≟ food = yes refl
food ≟ rent = no (λ ())
food ≟ fun = no (λ ())
food ≟ misc = no (λ ())
rent ≟ food = no (λ ())
rent ≟ rent = yes refl
rent ≟ fun = no (λ ())
rent ≟ misc = no (λ ())
fun ≟ food = no (λ ())
fun ≟ rent = no (λ ())
fun ≟ fun = yes refl
fun ≟ misc = no (λ ())
misc ≟ food = no (λ ())
misc ≟ rent = no (λ ())
misc ≟ fun = no (λ ())
misc ≟ misc = yes refl

enumerate : List Category
enumerate = food ∷ rent ∷ fun ∷ misc ∷ []

module Properties where
  open import Data.List.Membership.Setoid (categoryIsPropositionalSetoid) using (_∈_)
  open import Data.List.Relation.Unary.Any using (here; there)
  enumerateContainsAll : (c : Category) → c ∈ enumerate
  enumerateContainsAll food = here refl
  enumerateContainsAll rent = there $ here refl
  enumerateContainsAll fun = there $ there $ here refl
  enumerateContainsAll misc = there $ there $ there $ here refl

data Expense (c : Category): Set where
  mkExpense : ℕ → Expense c

record SomeExpense : Set where
  constructor mkSomeExpense
  field
    category : Category
    expense : Expense category

parseCategory : String -> Category
parseCategory "food" = food
parseCategory "rent" = rent
parseCategory "fun" = fun
parseCategory _ = misc

parseExpense : {c : Category} → String → Maybe (Expense c)
parseExpense = map mkExpense ∘ readMaybe 10
  where
    open import Data.Nat.Show
    open import Data.Maybe

parseSomeExpense : String → Maybe SomeExpense
parseSomeExpense str with String.words str
... | expenseStr ∷ categoryStr ∷ _ =
  mkSomeExpense (parseCategory categoryStr) <$> parseExpense expenseStr
  where
    open Data.Maybe renaming (map to _<$>_)
{-# CATCHALL #-}
... | _ = nothing

_+_ : {c : Category} → Expense c → Expense c → Expense c
mkExpense x + mkExpense y = mkExpense (x Data.Nat.+ y)

sum : {c : Category} → List (Expense c) → Expense c
sum = List.foldr _+_ (mkExpense 0)

filterExpense : (c : Category) -> List SomeExpense -> List (Expense c)
filterExpense _c [] = []
filterExpense c (mkSomeExpense d x ∷ xs) with c ≟ d
... | yes refl = x ∷ filterExpense c xs
... | no _ = filterExpense c xs

open import Data.Vec using (Vec; []; _∷_)
forM : {A B : Set} {n : ℕ} → Vec A n -> (A -> IO B) → IO (Vec B n)
forM [] _ = IO.return []
forM (x ∷ xs) f = _∷_ <$> f x <*> forM xs f
  where
    open import IO.Base
    open import Data.Vec

showCategory : Category → String
showCategory food = "food"
showCategory rent = "rent"
showCategory fun = "fun"
showCategory misc = "misc"

showExpense : {c : Category} → Expense c → String
showExpense {c} (mkExpense x) = Nat.show x ++ " " ++ showCategory c
  where
    open import Data.Nat.Show as Nat
    open import Data.String

main : Agda.Builtin.IO.IO ⊤
main = run $ do
  expenses <- mapMaybe parseSomeExpense ∘ lines <$> readFile "data"
  forM (Data.Vec.fromList enumerate) $ λ cat -> putStrLn ∘ showExpense ∘ sum ∘ filterExpense cat $ expenses
  return Data.Unit.tt
  where
    open import IO
    open import IO.Finite using (readFile)
```
